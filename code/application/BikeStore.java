package application;

import vehicles.Bicycle;

public class BikeStore {

    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("man1", 1, 20);
        bikes[1] = new Bicycle("man2", 2, 30);
        bikes[2] = new Bicycle("man3", 3, 40);
        bikes[3] = new Bicycle("man4", 4, 50);

        for (Bicycle b : bikes) {
            System.out.println(b);
        }

    }
}
